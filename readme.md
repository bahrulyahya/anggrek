##Git global setup
git config --global user.name "bahrul yahya"
git config --global user.email "bahrulyahya@gmail.com"

##Create a new repository
git clone git@gitlab.com:bahrulyahya/anggrek.git
cd anggrek
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

##Push an existing folder
cd existing_folder	-> maksudnya, pada command line kita harus masuk ke dalam folder project kita. Contoh : cd anggrek
git init
git remote add origin git@gitlab.com:bahrulyahya/anggrek.git
git add .
git commit -m "Initial commit"	-> maksudnya, di dalam tanda baca "" kita tulis pesan atau sesuatu ketika ada perubahan
git push -u origin master

##Push an existing Git repository
cd existing_repo
git remote rename origin old-origin
git remote add origin git@gitlab.com:bahrulyahya/anggrek.git
git push -u origin --all
git push -u origin --tags